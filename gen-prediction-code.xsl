<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">
  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template name="header">
    <![CDATA[
#!/usr/bin/env Rscript
## doc/tarot.org
## This file is part of tarot
## Copyright (C) 2019 Vivien Kraus <vivien@planete-kraus.eu>
##
## This program is free software: you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <http://www.gnu.org/licenses/>.

options (warn = 1)
data <- foreign::read.arff (file ("stdin"))

bid_levels <- c ("TAROT_PASS", "TAROT_TAKE", "TAROT_PUSH", "TAROT_STRAIGHT_KEEP", "TAROT_DOUBLE_KEEP")

data$bid_minimum <- factor (data$bid_minimum, bid_levels)
data$strategy_bid <- factor (data$strategy_bid, bid_levels)

TAROT_PASS <- 1
TAROT_TAKE <- 2
TAROT_PUSH <- 3
TAROT_STRAIGHT_KEEP <- 4
TAROT_DOUBLE_KEEP <- 5

prediction <- ]]>
  </xsl:template>

  <xsl:template name="footer">
    <![CDATA[
yhat <- prediction (data)

writeLines (as.character (yhat))]]>
  </xsl:template>

  <!-- Write a R function definition to vectorize the prediction -->
  <xsl:template match="tree">
    <xsl:call-template name="header" />
    <xsl:text>function (data) {&#xA;  sapply (seq_len (nrow (data)), function (i) {&#xA;    ret &lt;- 0&#xA;</xsl:text>
    <xsl:apply-templates />
    <xsl:text>    ret&#xA;  })&#xA;}</xsl:text>
    <xsl:call-template name="footer" />
  </xsl:template>

  <xsl:template match="split">
    <xsl:text>    if (data$`</xsl:text><xsl:value-of select="@variable" /><xsl:text>`[i] </xsl:text>
    <xsl:choose>
      <xsl:when test="@operator = '='">
	<xsl:text> %in% strsplit (&quot;</xsl:text>
	<xsl:value-of select="@value" />
	<xsl:text>&quot;, &quot;,&quot;)[[1]]) {&#xA;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="@operator" /><xsl:text> </xsl:text><xsl:value-of select="@value" /><xsl:text>) {&#xA;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>      ret &lt;- </xsl:text>
    <xsl:value-of select="@score" />
    <xsl:text>&#xA;    }&#xA;</xsl:text>
    <xsl:apply-templates />
  </xsl:template>

  <!-- Write the linear prediction -->
  <xsl:template match="linear-model">
    <xsl:call-template name="header" />
    <xsl:text>function (data) {&#xA;  (0&#xA;</xsl:text>
    <xsl:apply-templates />
    <xsl:text>  )&#xA;}&#xA;</xsl:text>
    <xsl:call-template name="footer" />
  </xsl:template>

  <xsl:template match="interaction">
    <xsl:text>    + </xsl:text><xsl:value-of select="@weight" /><xsl:text> * (1</xsl:text>
    <xsl:apply-templates />
    <xsl:text>)&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="variable[@name != '(Intercept)']">
    <xsl:text> * data$`</xsl:text><xsl:value-of select="@name" /><xsl:text>`</xsl:text>
  </xsl:template>

  <xsl:template match="variable[contains(@name, '_eq_')]">
    <xsl:text> * (data$`</xsl:text><xsl:value-of select="substring-before(@name, '_eq_')" /><xsl:text>`</xsl:text>
    <xsl:text> %in% strsplit (&quot;</xsl:text>
    <xsl:value-of select="substring-after(@name, '_eq_')" />
    <xsl:text>&quot;, &quot;,&quot;)[[1]])</xsl:text>
  </xsl:template>

  <xsl:template match="variable[contains(@name, '_leq_')]">
    <xsl:text> * (as.numeric (data$`</xsl:text>
    <xsl:value-of select="substring-before(@name, '_leq_')" />
    <xsl:text>`)</xsl:text>
    <xsl:text> &lt;= </xsl:text>
    <xsl:value-of select="substring-after(@name, '_leq_')" />
    <xsl:text>)</xsl:text>
  </xsl:template>
</xsl:stylesheet>
